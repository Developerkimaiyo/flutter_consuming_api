# Flutter: Widgets

This is a simple application that consumes api

### Version: 1.0.0


| Image 1    | Image 2     | Image 3     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619649199/states/9_bqxa5n.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827925/api/3_to4ddi.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827926/api/5_fhs6va.png" width="250"> |

| Image 4    | Image 5     | Image 6     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827928/api/2_lvl7p1.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827927/api/8_p0wp8b.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827926/api/6_phujhu.png" width="250"> |

| Image 7    | Image 8     | Image 9     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827926/api/4_g7vst9.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827925/api/1_rrcl7w.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619827926/api/7_kqds9k.png" width="250"> |


### Usage

```js
git clone  https://gitlab.com/Developerkimaiyo/flutter_consuming_api.git

```


### Support

Reach out to me at one of the following places!

- Twitter at <a href="http://twitter.com/maxxmalakwen" target="_blank">`@maxxmalakwen`</a>

Let me know if you have any questions. Email me At maxwell@sendyit.com or developerkimaiyo@gmail.com



---

### License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2021 © <a href="https://github.com/Developer-Kimaiyo" target="_blank">Maxwell Kimaiyo</a>.