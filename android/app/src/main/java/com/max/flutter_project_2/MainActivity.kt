package com.max.flutter_project_2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.SplashScreen

class MainActivity : FlutterActivity() {
    @Nullable
    override fun provideSplashScreen(): SplashScreen {
        return MySplashScreen() //Your Custom Splash Screen
    }
}

internal class MySplashScreen : SplashScreen {

    override fun createSplashView(context: Context, savedInstanceState: Bundle?): View? {
        return   LayoutInflater.from(context).inflate(R.layout.splash_view, null, false)
    }

    override fun transitionToFlutter(@NonNull onTransitionComplete: Runnable) {
        onTransitionComplete.run()
    }
}