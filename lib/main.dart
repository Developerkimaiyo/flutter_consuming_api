import 'package:flutter/material.dart';
import 'package:flutter_project_2/pages/detail.dart';
import 'package:flutter_project_2/pages/form.dart';
import 'package:flutter_project_2/pages/formItem.dart';
import 'package:flutter_project_2/providers/locationDetailsProvider.dart';
import 'package:flutter_project_2/providers/locationProvider.dart';
import 'package:provider/provider.dart';
void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => LocationProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => LocationDetailsProvider(),
        ),
      ],

      child: MaterialApp(
        title: 'Max Test',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch:  Colors.green,
        ),

        initialRoute: '/', // default is '/'
        routes: {
          '/FormPage': (ctx) => FormScreen(),
          '/formItem': (ctx) => FormItem(),
        },
        // ignore: missing_return
        onGenerateRoute: (settings) {
          print(settings.arguments);
        },
        onUnknownRoute: (settings) {
          return MaterialPageRoute(builder: (ctx) => FormScreen(),);
        },
      ),
    );
  }
}