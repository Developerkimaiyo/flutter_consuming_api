import 'package:flutter/material.dart';
import 'package:flutter_project_2/model/locationDetailsModel.dart';
import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;


class LocationDetailsProvider with ChangeNotifier {
  Place _place ;
  bool _loading = false;

  Place get place => _place;

  bool get loading => _loading;

  Future<void> getPlaceDetails(String placeId) async {
    _loading = true;
    notifyListeners();

    try {
      _place = (await loadPlaceDetails(placeId)).place;
      _loading = false;
      notifyListeners();
    } catch (e) {
      //handle error
      _loading = false;
      notifyListeners();
    }
  }
}

Future<PlaceDetails> loadPlaceDetails(String placeId) async {
  try {
    var _url =
        "https://maps.googleapis.com/maps/api/place/details/json?placeid=$placeId&key=AIzaSyCl5BLxUS-xQnv2UCuBIPtc5B83_Uozhbk";
    log(_url);
    var _res = await http.get(Uri.parse(_url));


    return PlaceDetails.fromJson(json.decode(_res.body));
  } catch (err) {
    throw 'Error:$err';
  }
}

