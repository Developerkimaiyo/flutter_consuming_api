import 'dart:convert' as convert;
import 'package:flutter/material.dart';
import 'package:flutter_project_2/model/locationModel.dart';
import 'package:http/http.dart' as http;


class LocationProvider with ChangeNotifier {

  Future<List<LocationModel>> getUserSuggestions(String query) async {
    final url = Uri.parse('https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=city%20of%20kenya&inputtype=textquery&fields=name,place_id,icon&key=AIzaSyCl5BLxUS-xQnv2UCuBIPtc5B83_Uozhbk');
    try {
      final response = await http.get(url);
      var json = convert.jsonDecode(response.body);
      var jsonResults = json['candidates'] as List;
      return jsonResults.map((json) => LocationModel.fromJson(json)).where((location) {

        final nameLower = location.name.toLowerCase();
        final queryLower = query.toLowerCase();
        return nameLower.contains(queryLower);
      }).toList();
    } catch (error) {
      throw (error);
    }
  }

}