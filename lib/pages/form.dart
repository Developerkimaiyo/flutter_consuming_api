import 'package:flutter/material.dart';
import 'package:flutter_project_2/model/locationModel.dart';
import 'package:flutter_project_2/providers/locationProvider.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:provider/provider.dart';


import 'formItem.dart';

  class FormScreen extends StatefulWidget {
  @override
  _FormScreenState createState() => _FormScreenState();
  }

  class _FormScreenState extends State<FormScreen> {
  static const routeName = '/formScreen';

  String id ="1";
  String title = "Great";
  Color color;


  TextEditingController nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final locationData = Provider.of<LocationProvider>(context);
    final locations = locationData.getUserSuggestions;
    final TextEditingController _controller = TextEditingController();
    void selectCategory(locationId) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => FormItem(
                placeId:locationId,
              )));

    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
        appBar: AppBar(
        title: Text('GoRider')),

      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: TypeAheadField(
            textFieldConfiguration: TextFieldConfiguration(
                style: TextStyle(
                  color: Colors.green,
                  fontWeight: FontWeight.bold,
                  fontSize: 26,
                ),
                autofocus: true,
                controller: _controller,
                keyboardType: TextInputType.text,
                enabled: true,

                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: BorderSide(
                      width: 6,

                      color: Colors.green[700],
                    ),
                  ),
                    prefixIcon: Icon(Icons.search),
                     hintText: 'Search location',
                  hintStyle: TextStyle(
                    color: Colors.green[700],
                  ),
                )
            ),
            getImmediateSuggestions: true,
            hideOnError: true,
            suggestionsCallback: locations,
            itemBuilder: (context,LocationModel  suggestion) {
              final locationModel = suggestion;

              return ListTile(
                leading: Icon(Icons.location_pin),
                title: Text(locationModel.name),
              );
            },
            noItemsFoundBuilder: (context) => Container(
              height: 100,
              child: Center(
                child: Text(
                  'No location Found.',
                  style: TextStyle(fontSize: 14),
                ),
              ),
            ),
            onSuggestionSelected: (LocationModel suggestion) {
              final locationModel = suggestion;
              selectCategory(locationModel.place_id);
              // ScaffoldMessenger.of(context)
              //   ..removeCurrentSnackBar()
              //   ..showSnackBar(SnackBar(
              //     content: Text('Selected LocationModel: ${locationModel.place_id}'),
              //   ));
            },
          ),
        ),
      )
    );
  }


}