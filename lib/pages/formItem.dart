import 'package:flutter/material.dart';
import 'package:flutter_project_2/providers/locationDetailsProvider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'dart:async';
class FormItem extends StatefulWidget {
  final String placeId;

  const FormItem({Key key, this.placeId}) : super(key: key);

  @override
  _FormItemState createState() => _FormItemState();
}

class _FormItemState extends State<FormItem> {
  @override
  void initState() {
    super.initState();

    Future.microtask(() =>
        context.read<LocationDetailsProvider>().getPlaceDetails(widget.placeId));
  }
   GoogleMapController mapController;

  Set<Marker> markers = Set();


  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  void _showMessage(lat,lng){
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
        content:Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(

              child: Text("Latitude :${lat}"),
            ),
            Expanded(

              child:   Text("Longitude : ${lng}")
            ),


          ],
        ),

      ));
  }
  @override
  Widget build(BuildContext context) {
    var state = context.watch<LocationDetailsProvider>();
     final LatLng  _center =  LatLng(state.place.geometry.location.lat!=null ? state.place.geometry.location.lat:0, state.place.geometry.location.lng!=null ? state.place.geometry.location.lng:0);
    markers.addAll([
      Marker(
          markerId: MarkerId('1'),
          position: LatLng(state.place.geometry.location.lat!=null ? state.place.geometry.location.lat:0, state.place.geometry.location.lng!=null ? state.place.geometry.location.lng:0)),
    ]);
     return new Scaffold(
      appBar: AppBar(
        title: Text(state.place.name),
        backgroundColor: Colors.green[700],
      ),
      body:Container(
    padding: EdgeInsets.all(2.0),
    margin: EdgeInsets.all(2.0),
    child: state.loading
    ? Center(
    child: CircularProgressIndicator(),
    )
        :
    GoogleMap(
        myLocationEnabled: true,
        onMapCreated: _onMapCreated,
        markers: markers,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 14.0,
        ),
      ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed:() => _showMessage(state.place.geometry.location.lat!=null ? state.place.geometry.location.lat:0, state.place.geometry.location.lng!=null ? state.place.geometry.location.lng:0),
        label: Text("Coordinates"),
        icon: Icon(Icons.location_pin),
      ),
    );
  }

}
