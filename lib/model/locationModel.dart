import 'package:flutter/material.dart';

class LocationModel  {
  final String name;
  final String place_id;
  final String geometry;


  LocationModel({
    this.name,
    this.place_id,
    this.geometry
  });
 static LocationModel fromJson(Map<String, dynamic> json) => LocationModel(
   name: json['name'], place_id:json['place_id'],geometry:json['geometry']
  );
}
